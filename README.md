# Gitlab CI Configs

These configs are meant for setups using my Ansible configs which deploy projects to Docker through [webhook-exec](https://github.com/tbmatuka/gowebhookexec).

## .env files

Each environment needs its own `.env.${ENVIRONMENT}` file by default. You can remove this requirement or change it to use a single `.env` file in all environments in the `.gitlab-ci.yml` file.

## Gitlab variables

Each project needs at least the `WEBHOOK_URL` variable to be defined in each environment. Without it, we can't deploy. You should never hardcode this URL into your `.gitlab-ci.yml` file, because that would make a huge security hole.

To pass any other secrets to your app, the default `build-project-config.sh` script will parse the `docker-app.env` looking for variables looking like `##VARIABLE_NAME##` and it will replace them with variables it gets from the CI.

**Make sure to scope your CI variables to the environment you want.**

## Cron jobs

Each service can have its own cron jobs. You can create a [crontab](https://crontab.guru/) file with a name like `${SERVICE}.cron` in the server config. You jobs will update automatically on deploy and will run inside your service's container.

## Running commands on web container start

My production Apache + PHP image runs `/usr/local/bin/apache_init.sh` before starting Apache. If you need to run migrations on deploy, this is the place to do it. Simply copy the `apache_init.sh` file into the image in your production `Dockerfile`.

## How does the deploy process work?

### Stage 1

Using the `tbmatuka/php-nodejs-build:${PHP_VERSION}` image, CI runs builds the app (composer and npm/yarn stuff), copies it to the `dist/` dir and saves that dir as an artifact.

### Stage 2

Using the [Kaniko](https://github.com/GoogleContainerTools/kaniko) image, CI takes the `dist/` artifact, builds an image from your `Dockerfile` (which should copy your app into your image) and stores that image in the Gitlab Docker registry.

### Stage 3

Using the `tbmatuka/ci-deploy` image, which only has Bash and Curl, CI builds your service config. The config is a dir which contains your `docker-compose.yml`, any docker env files, cron files and the `repository-auth.env` file which lets the server download the image from the Gitlab Docker registry. That dir gets packaged as a `.tar` file and sent to the server through a webhook.

### Server side

The server has [webhook-exec](https://github.com/tbmatuka/gowebhookexec) listening for your deploys. The deploy endpint will run the `update-project-config.sh` script and pass the body of the request to the script's `STDIN`. The script will unpack the `.tar` config into a dir, make some checks and copy all of the files that the server actually needs.

Once the new config is ready, it the script will trigger the next step, which is `deploy-docker-service.sh`. That script will create dirs for volumes, log into your Gitlab Docker registry, download the image(s) and deploy your `docker-compose.yml` file. Since the server is running Docker in swarm mode, only services whose config has changed will get deployed, the rest will keep running during the deploy.

**To avoid unnecessary restarts of your other services (for example redis), make sure your `docker-compose.yml` has a specific minor version set. Otherwise each deploy will pull a new version and restart the service.**
