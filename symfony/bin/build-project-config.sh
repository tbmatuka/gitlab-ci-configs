#!/bin/bash

set -e

DOCKER_CONFIG_DIR="$(realpath '.docker-config/production/')/"

DOCKER_COMPOSE_FILE='docker-compose.yml'
REPOSITORY_AUTH_FILE='repository-auth.env'
APP_ENV_FILE='docker-app.env'

PROJECT_CONFIG_FILE='project-config.tar'

CONFIG_DIR_NAME='server-config'

# set variables to dummy for local testing
if [ "$1" == "--local" ]
then
  CI_DEPLOY_USER=user
  CI_DEPLOY_PASSWORD=pass
  CI_REGISTRY=registry
  CI_REGISTRY_IMAGE=registry-image
  CI_COMMIT_REF_SLUG=ref-slug
fi

# check if all needed variables are set
for VAR_NAME in CI_DEPLOY_USER CI_DEPLOY_PASSWORD CI_REGISTRY CI_REGISTRY_IMAGE CI_COMMIT_REF_SLUG
do
  if [ -z "${!VAR_NAME}" ]
  then
    echo "Variable '\$${VAR_NAME}' is not set."

    exit 1
  fi
done

# prepare config dir
echo "Creating config dir: '${CONFIG_DIR_NAME}'"

rm -rf "${CONFIG_DIR_NAME}"
mkdir "${CONFIG_DIR_NAME}"
cd "${CONFIG_DIR_NAME}"

# copy files to config
cp "${DOCKER_CONFIG_DIR}${DOCKER_COMPOSE_FILE}" .
cp "${DOCKER_CONFIG_DIR}${REPOSITORY_AUTH_FILE}" .
find "${DOCKER_CONFIG_DIR}" -name 'docker-*.env' | xargs -I xxx cp xxx .
find "${DOCKER_CONFIG_DIR}" -name '*.cron' | xargs -I xxx cp xxx .

# add values to the repository auth file
for VAR_NAME in CI_DEPLOY_USER CI_DEPLOY_PASSWORD CI_REGISTRY
do
  sed -i "s|##${VAR_NAME}##|${!VAR_NAME}|" "${REPOSITORY_AUTH_FILE}"
done

# add values to the docker-compose file
for VAR_NAME in CI_REGISTRY_IMAGE CI_COMMIT_REF_SLUG
do
  sed -i "s|##${VAR_NAME}##|${!VAR_NAME}|" "${DOCKER_COMPOSE_FILE}"
done

# add values to the app env file
VARIABLE_NAMES=$(grep -Eo "##[^#]+##" "${APP_ENV_FILE}" | grep -v '##VARIABLE_NAMES##' | sed 's/##//g' | xargs echo)
for VAR_NAME in VARIABLE_NAMES ${VARIABLE_NAMES}
do
  sed -i "s|##${VAR_NAME}##|${!VAR_NAME}|" "${APP_ENV_FILE}"
done

# package config
echo "Creating config in '${PROJECT_CONFIG_FILE}'"
tar -cf "../${PROJECT_CONFIG_FILE}" .
