#!/bin/bash

# run migrations
./bin/console doctrine:migrations:migrate --allow-no-migration --no-interaction
